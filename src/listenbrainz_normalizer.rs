pub mod listenbrainz_normalizer {
    use std::{fs::File, str::FromStr, cmp::Reverse, time::{SystemTime, UNIX_EPOCH}, path::Path, io::{BufReader}, error::Error, collections::HashMap};
    use serde::{Deserialize, Serialize};
    use itertools::Itertools;
    use sqlite::{Connection, State};
    use uuid::Uuid;

    #[derive(Debug, Deserialize, Serialize)]
    pub struct Artist {
        name: String,
        total_duration: u64
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct Listen {
        track_metadata: TrackMetadata,
        listened_at: u64
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct TrackMetadata {
        artist_name: String,
        mbid_mapping: Option<MbidMapping>
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct MbidMapping {
        recording_mbid: Uuid
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Recording {
        id: Uuid,
        duration: u64
    }

    pub fn load_listens(path: String) -> Result<Vec<Listen>, Box<dyn Error>> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let result = serde_json::from_reader::<BufReader<File>, Vec<Listen>>(reader)?
            .into_iter()
            .filter(|e| e.track_metadata.mbid_mapping.is_some())
            .collect();

        Ok(result)
    }

    pub fn init_tracks_db(db_path: String, csv_path: Option<String>) -> Result<Connection, Box<dyn Error>> {
        let path = Path::new(&db_path);
        let exists = path.exists();

        let conn = Connection::open(path)?;

        if exists {
            return Ok(conn);
        }

        conn.execute("create table tracks (id text primary key, duration integer)")?;

        let tracks = &parse_tracks_db(csv_path.unwrap())?;

        let chunk_size = 10000;
        let chunks_count = tracks.len() / chunk_size + 1;
        let last_chunk_size = tracks.len() % chunk_size;
        for chunk in 1..chunks_count {
            let size = if chunk == chunks_count { last_chunk_size } else { chunk_size };
            let values = tracks.into_iter().skip((chunk-1) * chunk_size).take(size).map(|track| {
                let id = track.id;
                let duration = track.duration;
                format!("('{id}', {duration})")
            }).join(", ");
        
            let query = format!("insert into tracks values {values}");
            conn.execute(query)?;    
        }

        Ok(conn)
    }

    fn get_track_duration(conn: &Connection, id: &Uuid) -> Result<u64, Box<dyn Error>> {
        let mut result: u64 = 0;
        let id = Uuid::to_string(id);
        let query = format!("select duration from tracks where id = ?");

        let mut statement = conn.prepare(query)?;
        statement.bind((1, &id.to_owned()[..]))?;

        while let Ok(State::Row) = statement.next() {
            result = statement.read::<i64, _>("duration")? as u64;
        }

        Ok(result)
    }

    fn parse_tracks_db(path: String) -> Result<Vec<Recording>, Box<dyn Error>> {
        let mut result = Vec::new();
        let mut reader = csv::ReaderBuilder::new().delimiter(b'\t').flexible(true).from_path(path)?;

        for track in reader.records() {
            let track = track?;
            if track.len() != 9 {
                continue;
            }

            let duration = &track[4];
            if duration == "\\N" {
                continue;
            }

            let rec = Recording {
                duration: u64::from_str(duration)?,
                id: Uuid::from_str(&track[1])?
            };

            result.push(rec);
        }
        
        Ok(result)
    }

    pub fn get_stats(listens: Vec<Listen>, tracks: &Connection, months: u64) -> Result<Vec<Artist>, Box<dyn Error>> {
        let mut result = Vec::new();

        let seconds: u64 = months*30*24*60*60;
        let date = SystemTime::now().duration_since(UNIX_EPOCH).expect("time error").as_secs();

        let mut artists = HashMap::<String, Vec<&Listen>>::new();
        for listen in listens.iter().filter(|e| e.listened_at > date - seconds) {
            let key = listen.track_metadata.artist_name.to_string();
            if artists.contains_key(&key) {
                artists.get_mut(&key).unwrap().push(&listen);
            } else {
                artists.insert(key, vec![&listen]);
            }
        }

        for artist in artists.into_iter() {
            let total_duration: u64 = artist.1.iter()
                .map(|listen| get_track_duration(tracks, &listen.track_metadata.mbid_mapping.as_ref().unwrap().recording_mbid).unwrap())
                .sum();

            result.push(Artist {
                name: artist.0.to_string(),
                total_duration: total_duration
            });
        }

        result.sort_by_key(|a| Reverse(a.total_duration));

        Ok(result)
    }
}

