mod listenbrainz_normalizer;

use std::error::Error;
use clap::Parser;

use crate::listenbrainz_normalizer::listenbrainz_normalizer::{load_listens, init_tracks_db, get_stats};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    listens: String,

    #[arg(long, default_value = None)]
    tracks_src: Option<String>,

    #[arg(short, long)]
    tracks: String,

    #[arg(short, long)]
    months: u64
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();
    
    let listens = load_listens(args.listens)?;
    let tracks = init_tracks_db(args.tracks, args.tracks_src)?;

    let stats = get_stats(listens, &tracks, args.months)?;

    let stats_json = serde_json::to_string_pretty(&stats)?;
    println!("{stats_json}");

    Ok(())
}
