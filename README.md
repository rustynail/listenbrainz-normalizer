# listenbrainz-normalizer

This is an attempt to recreate a now defunct last.fm normalizer tool which makes a list of your top artists based on actual time played.  
Except it uses listenbrainz listens instead of last.fm scrobbles. Listenbrainz can import listens from last.fm and apparently also spotify (didn't try that).

# requirements

You will have to manually export your listens as json from https://listenbrainz.org/profile/export/  
  
Also you have to download the musicbrainz database snapshot for song durations https://musicbrainz.org/doc/MusicBrainz_Database/Download  
The only download required is mbdump.tar.bz2 (about 16gb unpacked), and the only file you need from the archive is mbdump/recording.  

# usage

listenbrainz-normalizer --listens ./listens.json --tracks ./tracks.db --tracks_src ./recording --months 12  
  
--listens is your listenbrainz json.  
--tracks is a tracks lengths database which is not going to exist on the first launch and will take a while to generate.  
--tracks_src is the "recording" file from musicbrainz database snapshot (only required on the first launch).  
--months is a period of listening time.   

